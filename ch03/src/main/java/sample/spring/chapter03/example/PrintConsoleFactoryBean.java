package sample.spring.chapter03.example;

import java.io.InputStream;
import java.util.Properties;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.core.io.ClassPathResource;

public class PrintConsoleFactoryBean implements FactoryBean<PrintConsole> {

	private String printTextPropertiesFile;
	
	
	public String getPrintTextPropertiesFile() {
		return printTextPropertiesFile;
	}

	public void setPrintTextPropertiesFile(String printTextPropertiesFile) {
		this.printTextPropertiesFile = printTextPropertiesFile;
	}

	@Override
	public PrintConsole getObject() throws Exception {
		// TODO Auto-generated method stub
		System.out.println("getObject() invoked");
		PrintConsole retval = null;
		Properties properties = new Properties();
		
		ClassPathResource textProperties = null;
		if(printTextPropertiesFile != null){
			textProperties = new ClassPathResource(printTextPropertiesFile);
		}
		if(textProperties != null && textProperties.exists()){
			InputStream inStream = textProperties.getInputStream();
			properties.load(inStream);
			retval = new PrintConsoleImpl(properties);
		}
		
		return retval;
	}

	@Override
	public Class<?> getObjectType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isSingleton() {
		// TODO Auto-generated method stub
		return false;
	}

}
