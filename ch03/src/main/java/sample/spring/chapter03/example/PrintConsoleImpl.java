package sample.spring.chapter03.example;

import java.util.Properties;
import java.util.Scanner;

public class PrintConsoleImpl implements PrintConsole{
	private Properties textTypeProperties;

	public PrintConsoleImpl(){
		
	}
	public PrintConsoleImpl(Properties textTypeProperties) {
		this.textTypeProperties = textTypeProperties;
	}

	public Properties getTextTypeProperties() {
		return textTypeProperties;
	}

	public void setTextTypeProperties(Properties textTypeProperties) {
		this.textTypeProperties = textTypeProperties;
	}

	@Override
	public void PrintMessage() {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("pleas select print text (1~3) : ");
		input.reset();
		String type = input.next();
		String ENText = (String) textTypeProperties.get("text"+type);
		System.out.println(ENText);
	}

}
